//
// Created by Romain on 19/07/2024.
//

#ifndef GREEDYALGORITHM_GLOBALS_H
#define GREEDYALGORITHM_GLOBALS_H

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>
#define assertm(exp, msg) assert(((void)msg, exp))

using uint = unsigned int;
using ulong = unsigned long;

#endif //GREEDYALGORITHM_GLOBALS_H
