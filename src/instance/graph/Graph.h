//
// Created by rbernard on 22/01/24.
//

#ifndef GREEDYALGORITHM_GRAPH_H
#define GREEDYALGORITHM_GRAPH_H


#include <utility>
#include <string>
#include <random>
#include <filesystem>

#include "Node.h"
#include "Edge.h"
#include "../../utils/Globals.h"
#include "../../utils/Constants.h"
#include "../../algorithm/ShortestPath/Vehicle/VehicleShortestPathCalculation.h"
#include "../../algorithm/ShortestPath/Transit/TransitShortestPathContainer.h"
#include "../../algorithm/ShortestPath/Transit/TransitShortestPathPrecompute.h"

class DATRow;
class Graph {
private:
    std::vector<Node> nodesVector; //The full list of nodes created in the graph
    std::vector<Edge> edgesVector;
    std::vector<Line> transitLines;
    std::vector<std::vector<uint>> shortestSAEVPaths;
    TransitShortestPathContainer transitShortestPaths = TransitShortestPathContainer(0);
    size_t _depotNodeIdx{0}; //Index of the depot node, defaults to 0, the first node created in the graph

    /**
     * For every LineStop on every node of the graph, verify the node returned by looking into LineStop->Line(stopIdx)
     * is the same as the node where the LineStop is defined
     * @return True if @this is properly referenced in the Line object at the expected index
     */
    bool checkLineToNodeLinks();
    /**
     * Parses and adds a node from the given row object to the graph.<br>
     * Format : status,x_coordinate,y_coordinate
     * @param row row extracted from the input file
     */
    void parseNodeRow(const DATRow& row);
    /**
     * Parses and adds an edge from the given row object to the graph.<br>
     * Format : start_node_idx,end_node_idx,length
     * @param row row extracted from the input file
     */
    void parseEdgeRow(const DATRow& row);

    /**
     * Parses and adds a line to the graph from a given row object and reusing the given seeded rng engine.
     * Uses the format : <br>
     * frequency,start_time,end_time,min_travel_time,max_travel_time,node_0, ..., node_n
     * @param row row containing the necessary info to generate a random line
     * @param rng seeded random number generator
     */
    void parseLineRandomizedSchedule(const DATRow &row, std::mt19937 rng);

    /**
     * Parses and adds a line to the graph from the given row in given the following format : <br>
     * node_0_idx, ... node_n_idx <br>
     * node_0_pass_0_time, ... node_n_pass_0_time <br>
     * ... <br>
     * node_0_pass_m_time, ... node_n_pass_m_time <br>
     * <br>
     * /!\ each transit line to parse ends with an empty line
     *
     * @param infile input file stream, necessary to iterate over multiple lines in the function
     * @param row the current DATrow object we reuse from other parsing functions
     */
    void parseLineFixedSchedule(std::ifstream& infile, DATRow& row);

public:
    [[nodiscard]] const std::vector<Node> &getNodesVector() const {
        return nodesVector;
    }

    [[nodiscard]] const Node &getNode(size_t nodeIndex) const {
        return nodesVector[nodeIndex];
    }

    [[nodiscard]] size_t getNbNodes() const {
        return nodesVector.size();
    }

    [[nodiscard]] const std::vector<LineStop> & getPTLinesSet(size_t nodeIndex) const {
        return nodesVector[nodeIndex].getPTLinesSet();
    }

    [[nodiscard]] size_t getNbPTLines(size_t nodeIndex) const {
        return nodesVector[nodeIndex].getPTLinesSet().size();
    }

    [[nodiscard]] size_t getDepotNodeIdx() const;
    void setDepotNodeIdx(size_t depotNodeIdx);

    /**
     * @return The graph's edge vector
     */
    [[nodiscard]] const std::vector<Edge> &getEdgesVector() const {
        return edgesVector;
    }
    /**
     * @return Const ref to the edge at the given index
     */
    [[nodiscard]] const Edge &getEdge(size_t edgeIndex) const {
        return edgesVector[edgeIndex];
    }

    [[nodiscard]] size_t getNbIncomingEdges(size_t nodeIndex) const {
        return nodesVector[nodeIndex].getIncomingEdges().size();
    }

    [[nodiscard]] size_t getNbOutgoingEdges(size_t nodeIndex) const {
        return nodesVector[nodeIndex].getOutgoingEdges().size();
    }

    [[nodiscard]] size_t getNbEdges(size_t nodeIndex) const {
        return nodesVector[nodeIndex].getIncomingEdges().size() + nodesVector[nodeIndex].getOutgoingEdges().size();
    }

    /**
     * @return A vector containing the list of transit lines defined in our graph (effectively G^TP)
     */
    [[nodiscard]] const std::vector<Line> &getPTLines() const {
        return transitLines;
    }

    /**
     * Pushes a reference to the given edge at the end of the graph's edge vector
     * @param edge The edge to push in the graph's edge vector
     * @return The edge vector after having added the edge
     */
    const std::vector<Edge>& addEdge(Edge const& edge) {
        edgesVector.push_back(edge);
        return edgesVector;
    }

    /**
     * Pushes a reference to the given node at the end of the graph's node vector
     * @param node The node to push in the graph's node vector
     * @return The node vector after having added the node
     */
    const std::vector<Node>& addNode(Node const& node) {
        nodesVector.push_back(node);
        return nodesVector;
    }

    /**
     * Creates a Graph and fills it with data from a single dat file with :
     * nodes, edges and public transit data. Each separated by a #comment line.
     * Each object is a line in the file
     * @param datFilePath relative or absolute path to the dat file to import objects from
     */
    explicit Graph(const std::string& datFilePath);
    /**
     * Creates a Graph and fills it with data from individual files for each object type :
     * nodes, edges and public transit data. Lines starting by # will be considered comments and skipped.
     * Each object is a line in the file
     * @param nodeFilePath The file containing node data (format for each line : status,x,y)
     * @param edgeFilePath The file containing edge data (format for each line : start_node_index,end_node_index,edge_length)
     * @param ptLineFilePath
     */
    [[deprecated("Replaced with the single file syntax for creating a graph via file")]]
    Graph(const std::string& nodeFilePath, const std::string& edgeFilePath, const std::string& ptLineFilePath);

    /**
     * Adds a new Line to the graph.
     * @param line The Line to add. It's expected to be fully filled with its nodes schedules before adding to the graph
     * @return The updated vector containing all lines of the graph
     */
    const std::vector<Line>& addLine(const Line &line);
    /**
     * Export graph data (nodes, edges, transit lines) to files to plot graph data with Python
     * The function creates the folders if need be, and will overwrite existing files if data has been outputted to this folder before
     * @param exportFolderPath A path to a folder where we can export edges.txt, nodes.txt and ptlines.txt
     */
    void exportGraphToFile(const std::filesystem::path& exportFolderPath);

    /**
     * Executes defined check functions on every node and transit line in the graph
     * @return True if all checks were successful, false otherwise
     */
    bool check();

    /**
     * Adds a new edge at the back of the edgesVector, and properly links this edge to its entry and exit nodes
     * @param edgeStartNodeIndex Index of the node serving as a starting point for this edge
     * @param edgeEndNodeIndex Index of the node serving as an exit point for this edge
     * @param length The length of this edge (in minutes)
     */
    void createAndAddEdge(size_t edgeStartNodeIndex, size_t edgeEndNodeIndex, double length);

    [[nodiscard]] const std::vector<std::vector<uint>> &getShortestSaevPaths() const;

    void setShortestSaevPaths(const std::vector<std::vector<uint>> &shortestSaevPaths);

    [[nodiscard]] uint getShortestSAEVPath(size_t startingNodeIdx, size_t destinationNodeIdx) const { return shortestSAEVPaths[startingNodeIdx][destinationNodeIdx]; }

    void parseDistanceMatrix(std::ifstream &infile, DATRow currentRow);

    void emplaceBackClosestStation(size_t nodeIdx, size_t stationNodeIdx);

    const size_t getNbClosestStations(size_t nodeIdx);

    void computeAndUpdateShortestPathsMatrix() {
        VehicleShortestPathCalculation::computeAndUpdateShortestPathsForGraph(*this, false);
    }

    void computeAndUpdateShortestPathsMatrix(bool useEdges) {
        VehicleShortestPathCalculation::computeAndUpdateShortestPathsForGraph(*this, useEdges);
    }

    void computeAndUpdateClosestStationsForNode(size_t nodeIdx);
    void computeAndUpdateClosestStations();
    void computeAndUpdateShortestTransitPaths();

    [[nodiscard]] const TransitShortestPathContainer &getTransitShortestPaths() const;
    const std::pair<size_t, std::vector<TransitShortestPath>>& getShortestTransitPathsFrom(size_t startNodeIndex, uint earliestStartInstant) const;
    TransitShortestPath getShortestTransitPathToYFromTime(size_t startNodeIndex, uint earliestStartInstant, size_t goalNode) const;

    void linkAllPTNodes();
};


#endif //GREEDYALGORITHM_GRAPH_H
