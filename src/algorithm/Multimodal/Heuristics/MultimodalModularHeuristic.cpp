//
// Created by romain on 15/07/24.
//

#include "MultimodalModularHeuristic.h"

using transit_order_function = std::function<bool(MultimodalModularHeuristic::ScoredTransitAccess, MultimodalModularHeuristic::ScoredTransitAccess)>;

std::vector<TransitAccess> MultimodalModularHeuristic::getBestTransitEntriesList(const Request &baseRequest) const {
    const auto& bestStationsIndexVector = _graph->getNode(
            baseRequest.getOriginNodeIndex()).getBestStationsNodeIdxVector();
    std::vector<TransitAccess> results; //init results vector to the appropriate size
    results.reserve(std::min(Constants::MAX_TRANSIT_ENTRY_CANDIDATES, bestStationsIndexVector.size()));
    //Iterate over the best stations saved prior
    for(const auto& bestStationNodeIdx : bestStationsIndexVector) {
        int maxDepartureTime = -1;

        //Iterate over the lines available on the node selected
        for(const Node& bestStationNode = _graph->getNode(bestStationNodeIdx);
            const auto& lineStop : bestStationNode.getPTLinesSet()) {
            //Find the next passage lower or equal to our max entry time constraint
            auto iterator = lineStop.findNextScheduledPassage(getMaxEntryConstraint(baseRequest, bestStationNodeIdx));
            if(iterator != lineStop.getSchedule().cbegin()) { //Iterator is invalid if it points to schedule beginning (no result lower than our constraint)
                --iterator; //Move iterator to the value that's under our max entry constraint
                if(*iterator > maxDepartureTime //If we've found a valid time that's superior to our current max time
                && *iterator > getMinEntryConstraint(baseRequest, bestStationNodeIdx)) { //and respects min entry time, replace old value
                    maxDepartureTime = *iterator;
                }
            }
        }

        //If we've found a valid max departure time for this station, add it to the list
        if(maxDepartureTime > -1) {
            results.emplace_back(bestStationNodeIdx, maxDepartureTime);
            if(results.size() == Constants::MAX_TRANSIT_ENTRY_CANDIDATES) {
                break;
            }
        }
    }
    return results;
}

uint MultimodalModularHeuristic::getMinEntryConstraint(const Request &request, size_t ptEntryNodeIdx) const {
    return request.getMinDepartureTw() + _graph->getShortestSAEVPath(request.getOriginNodeIndex(), ptEntryNodeIdx);
}

uint MultimodalModularHeuristic::getMaxEntryConstraint(const Request &request, size_t ptEntryNodeIdx) const {
    return request.getMaxArrivalTw() - ((uint) std::floor(_graph->getShortestSAEVPath(ptEntryNodeIdx, request.getDestinationNodeIndex()) * request.getTransitTravelTimeRatio()));
}

/**
 * Generates the best entries list (ordered by an implementation-dependant ,
 * then generates a vector of requests from these entries before starting the entry subrequest insertion process <br>
 * The insertion process first tries best insertions without creating a new vehicles in order of the best entries list.
 * If none result in a valid insertion, we insert the first subrequest (supposedly the better one) in a new vehicle
 * @param baseRequest const ref to the request we use as our base to get the best entries list
 * @param baseRequestId ID/index in the request vector for our base request
 * @return The subrequest successfully inserted in our route. This method's caller needs to add this request to its main request vector
 */
const Request & MultimodalModularHeuristic::insertBestTransitEntryInRoute(const Request &baseRequest, size_t baseRequestId) {
    std::vector<TransitAccess> entriesAccessList = getBestTransitEntriesList(baseRequest);
    return insertBestTransitAccessInRoute(baseRequest, entriesAccessList, baseRequestId, true);
}

/**
 * Generates a vector of requests from the given access list before starting the entry subrequest insertion process <br>
 * The insertion process first tries best insertions without creating a new vehicles in order of the best entries list.
 * If none result in a valid insertion, we insert the first subrequest (supposedly the better one) in a new vehicle
 * @param baseRequest const ref to the request we use as our base to generate subrequests from the sorted best access list
 * @param transitAccessList A list of best entries, preferably ordered from best to worst and sized according to the max number of candidates we want to try inserting
 * @param baseRequestId ID/index in the request vector for our base request
 * @return The subrequest successfully inserted in our route. This method's caller needs to add this request to its main request vector
 */
const Request &
MultimodalModularHeuristic::insertBestTransitAccessInRoute(const Request &baseRequest,
                                                       const std::vector<TransitAccess> &transitAccessList,
                                                       size_t baseRequestId, bool isEntry) {
    std::vector<Request> accessSubRequestsList;
    size_t maxSize = isEntry ? Constants::MAX_TRANSIT_ENTRY_CANDIDATES : Constants::MAX_TRANSIT_EXIT_CANDIDATES;
    accessSubRequestsList.reserve(maxSize); //Init entry subrequests list to the appropriate size
    //Generate subrequests from best transit entries
    for(auto const& access : transitAccessList) {
        try {
            if(isEntry)
                accessSubRequestsList.emplace_back(*_graph, baseRequest, access);
            else
                accessSubRequestsList.emplace_back(*_graph, baseRequest, access,
                                                   _route->getEntrySubRequestOrigin(baseRequestId));
        }
        catch(const TimeWindow::invalid_time_window_exception& e) {
            DEBUG_MMH_MSG("Invalid Time Window during candidate sub request creation, it won't be added to the list");
        }
    }
    return insertBestTransitAccessInRoute(accessSubRequestsList, baseRequestId, isEntry);
}

/**
 * The insertion process first tries best insertions without creating a new vehicles in order of the best entries list. <br>
 * If none result in a valid insertion, we insert the first subrequest (supposedly the better one) in a new vehicle. <br>
 * While doing these insertions, the route and global request vector is updated with the appropriate request so that no inconsistencies in data structures happen
 * @param accessSubRequestsList A list of entry subrequest candidates, preferably ordered from best to worst candidate, but the list order is implementation dependant
 * @param baseRequestId ID/index in the request vector for our base request
 * @param isEntry true iff the given access requests are transit entry requests
 * @return The subrequest successfully inserted in our route
 */
const Request &
MultimodalModularHeuristic::insertBestTransitAccessInRoute(const std::vector<Request> &accessSubRequestsList,
                                                       size_t baseRequestId, bool isEntry) {
    for(const auto& subreq : accessSubRequestsList) {
        updateSubRequest(baseRequestId, subreq, isEntry);
        SAEVRouteChangelist changeList = BestInsertionHeuristic::tryBestRequestInsertionInActiveVehicle(
                _route->getSubRequestOrigin(baseRequestId, isEntry), *_route);
        //If we've found an insertion that doesn't create a vehicle, stop there
        if(changeList.success()) {
            DEBUG_MMH_MSG(std::string(isEntry ? "ENTRY" : "EXIT") + " CANDIDATE SUCCESS : " + subreq.to_string());
            return getSubrequest(baseRequestId, isEntry);
        } else {
            DEBUG_MMH_MSG(std::string(isEntry ? "ENTRY" : "EXIT") + " CANDIDATE FAILURE : " + subreq.to_string() + "\n"
                        + "CAUSE : " + changeList.getStatusString());
        }
    }

    // If no valid candidate was given, still create a fake
    // subrequest and mention failure in the appropriate vector
    if(accessSubRequestsList.empty()) {
        DEBUG_MMH_MSG(std::string("UNFULFILLED SUBREQUEST ") + (isEntry ? "ENTRY" : "EXIT"));
        updateSubRequest(baseRequestId, (*_requestsVect)[baseRequestId], isEntry);
        updateUnfulfilledSubrequest(baseRequestId, isEntry, true); //mark failures for debug/statistics
        if(!isEntry) { //Exit subrequest : remove entry subrequest and add baseRequest to the "unfulfilled list"
            _route->removeRequestWithPropagation(baseRequestId, true);
        }
        emplaceDARPRequest(baseRequestId);
    } else {
        // If no active vehicle insertion worked, do best insertion on a new vehicle
        // with the first subrequest (supposedly it's the most advantageous)
        DEBUG_MMH_MSG("CREATE VEHICLE");
        updateSubRequest(baseRequestId, accessSubRequestsList[0], isEntry);
        _route->insertRequestInNewVehicle(_route->getExitSubRequestOrigin(baseRequestId));
    }

    return getSubrequest(baseRequestId, isEntry);
}

const Graph *MultimodalModularHeuristic::getGraph() const {
    return _graph;
}

void MultimodalModularHeuristic::setGraph(const Graph *graph) {
    _graph = graph;
}

SAEVRoute *MultimodalModularHeuristic::getRoute() const {
    return _route;
}

void MultimodalModularHeuristic::setRoute(SAEVRoute *route) {
    _route = route;
}

void MultimodalModularHeuristic::updateSubRequest(size_t requestId, const Request &request, bool isEntry) {
    if(isEntry)
        _entrySubRequests[requestId] = request;
    else
        _exitSubRequests[requestId] = request;

    if(isEntry) {
        _route->getEntrySubRequestOrigin(requestId).setRequest(&getSubrequest(requestId, isEntry));
        _route->getEntrySubRequestDestination(requestId).setRequest(&getSubrequest(requestId, isEntry));
    } else {
        _route->getExitSubRequestOrigin(requestId).setRequest(&getSubrequest(requestId, isEntry));
        _route->getExitSubRequestDestination(requestId).setRequest(&getSubrequest(requestId, isEntry));
    }
}

const Request& MultimodalModularHeuristic::getSubrequest(size_t requestId, bool isEntry) {
    return isEntry ? _entrySubRequests[requestId] : _exitSubRequests[requestId];
}

double MultimodalModularHeuristic::getTransitExitScore(const Request &baseRequest, const TransitAccess &exitData) const {
    return getTransitExitScore(exitData.getAccessNodeIdx(), baseRequest.getDestinationNodeIndex(), exitData.getAccessTimestamp());
}

double MultimodalModularHeuristic::getTransitExitScore(size_t transitExitNodeIndex, size_t requestDestinationNodeIndex, uint transitExitTimestamp) const {
    return _graph->getShortestSAEVPath(transitExitNodeIndex, requestDestinationNodeIndex) + transitExitTimestamp;
}

transit_order_function
MultimodalModularHeuristic::getScoredTransitExitOrderer() {
    return [](MultimodalModularHeuristic::ScoredTransitAccess lhs, MultimodalModularHeuristic::ScoredTransitAccess rhs) { return lhs.score < rhs.score; };
}

uint MultimodalModularHeuristic::getMinExitConstraint(size_t baseRequestId, const SAEVKeyPoint &entrySubRequestOriginKP,
                                                  size_t transitExitNodeIdx) const {
    return 0;
}

uint MultimodalModularHeuristic::getMaxExitConstraint(size_t baseRequestId, const SAEVKeyPoint &entrySubRequestOriginKP,
                                                  size_t transitExitNodeIdx) const {
    const Request& baseRequest = (*_requestsVect)[baseRequestId];
    return (entrySubRequestOriginKP.getMinTw() + baseRequest.getDeltaTime()) -
           _graph->getShortestSAEVPath(transitExitNodeIdx, baseRequest.getDestinationNodeIndex());
}

std::vector<TransitAccess>
MultimodalModularHeuristic::getBestTransitExitsList(size_t baseRequestId) {
    const Request& baseRequest = (*_requestsVect)[baseRequestId];
    const SAEVKeyPoint& entrySubRequestOriginKP = _route->getEntrySubRequestOrigin(baseRequestId);
    return getBestTransitExitsList(baseRequestId, baseRequest, entrySubRequestOriginKP);
}

std::vector<TransitAccess>
MultimodalModularHeuristic::getBestTransitExitsList(size_t baseRequestId, const Request &baseRequest,
                                                const SAEVKeyPoint &entrySubRequestOriginKP) const {
    std::vector<MultimodalModularHeuristic::ScoredTransitAccess> scoredTransitExits;
    //Get departure time/shortest transit paths list from the entry sub request's max time (this means we take the first transit available after current max arrival)
    //TODO : study other approaches (e.g check for a faster max arrival if it's valid and allows better paths. This would require propagation => costly)

    size_t entryNodeIdx = entrySubRequestOriginKP.getCounterpart()->getNodeIndex();
    uint transitMinDepartureTime = entrySubRequestOriginKP.getCounterpart()->getMaxTw();
    if(entrySubRequestOriginKP.getNodeIndex() == entrySubRequestOriginKP.getCounterpart()->getNodeIndex()) {
        transitMinDepartureTime = baseRequest.getMinDepartureTw(); //TODO: check with the team if this choice is fine
    }
    const auto& [departureTime, shortestTransitPaths] = _graph->getShortestTransitPathsFrom(entryNodeIdx,
                                                                                            transitMinDepartureTime);

    //Iterate over the best stations saved prior
    for(const auto& shortestTransitPath : shortestTransitPaths) {
        //Check valid transit path + arrival node != starting point
        if(shortestTransitPath.getArrivalTime() >= 0 && shortestTransitPath.getArrivalNode() != entryNodeIdx
        && shortestTransitPath.getArrivalTime() <= getMaxExitConstraint(baseRequestId, entrySubRequestOriginKP, shortestTransitPath.getArrivalNode())) {
            TransitAccess exit{shortestTransitPath.getArrivalNode(),  (uint) shortestTransitPath.getArrivalTime()};
            scoredTransitExits.emplace_back(exit, getTransitExitScore(baseRequest, exit));
        }
    }

    //Sort and truncate transit exits list while removing score data that's unnecessary in later steps
    std::ranges::sort(scoredTransitExits, getScoredTransitExitOrderer());
    uint finalVectorSize = std::min(scoredTransitExits.size(), Constants::MAX_TRANSIT_EXIT_CANDIDATES);
    std::vector<TransitAccess> truncatedTransitExitsList{scoredTransitExits.begin(), scoredTransitExits.begin() + finalVectorSize};
    return truncatedTransitExitsList;
}

const Request & MultimodalModularHeuristic::insertBestTransitExitsInRoute(const Request &baseRequest, size_t baseRequestId) {
    std::vector<TransitAccess> exitAccessList = getBestTransitExitsList(baseRequestId);
    return insertBestTransitAccessInRoute(baseRequest, exitAccessList, baseRequestId, false);
}

void MultimodalModularHeuristic::updateUnfulfilledSubrequest(size_t baseRequestId, bool isEntry, bool value) {
    if(isEntry) {
        _unfulfilledTransitEntry[baseRequestId] = value;
    } else {
        _unfulfilledTransitExit[baseRequestId] = value;
    }
}

const std::vector<bool> &MultimodalModularHeuristic::getUnfulfilledTransitExit() const {
    return _unfulfilledTransitExit;
}

const std::vector<bool> &MultimodalModularHeuristic::getUnfulfilledTransitEntry() const {
    return _unfulfilledTransitEntry;
}

bool MultimodalModularHeuristic::isEntryFulfilled(size_t baseRequestId) {
    return !_unfulfilledTransitEntry[baseRequestId];
}

const std::vector<Request> &MultimodalModularHeuristic::getEntrySubRequests() const {
    return _entrySubRequests;
}

const std::vector<Request> &MultimodalModularHeuristic::getExitSubRequests() const {
    return _exitSubRequests;
}

void MultimodalModularHeuristic::emplaceDARPRequest(size_t baseRequestId) {
    _darpRequestList.emplace_back(baseRequestId);
}

void MultimodalModularHeuristic::doMultimodalRequestsInsertion() {
    // Step 1
    // Execute transit entry insertion procedure first, it'll mark every failed request
    // and greedily insert the best compatible entry request
    for(size_t requestId = 0; requestId < _requestsVect->size(); ++requestId) {
        const Request& baseRequest = (*_requestsVect)[requestId];
        if(baseRequest.isMultimodal())
            insertBestTransitEntryInRoute(baseRequest, requestId);
        else
            emplaceDARPRequest(requestId);
    }

    // Step 2
    // If the entry request was fulfilled, do the exit insertion procedure
    // if this step fails (no compatible exit) undoes the entry insertion, which is a costly process
    for(size_t requestId = 0; requestId < _requestsVect->size(); ++requestId) {
        if(!_unfulfilledTransitEntry[requestId])
            insertBestTransitExitsInRoute((*_requestsVect)[requestId], requestId);
    }

    // Step 3
    // Do best request insertion for every unfulfilled multimodal request and non-multimodal request
    for(size_t requestId : _darpRequestList) {
        BestInsertionHeuristic::doBestRequestInsertionForRoute(_route->getRequestOrigin(requestId), *_route);
    }
}
