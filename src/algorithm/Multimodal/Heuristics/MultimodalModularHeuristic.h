//
// Created by romain on 15/07/24.
//

#ifndef GREEDYALGORITHM_MULTIMODALMODULARHEURISTIC_H
#define GREEDYALGORITHM_MULTIMODALMODULARHEURISTIC_H


#include <cstddef>
#include <vector>
#include <float.h>
#include "../../../instance/requests/Request.h"
#include "../../../routes/vehicle/SAEVRoute.h"
#include "TransitAccess.h"
#include "../../DARP/Heuristics/BestInsertionHeuristic.h"
#include "../../../../test/lib/googletest/googletest/include/gtest/gtest_prod.h"

#ifdef DEBUG_MULTIMODAL_HEURISTIC
#include <iostream>
#define DEBUG_MMH_MSG(str) do { std::cout << "[MMH] " << str << std::endl; } while( false )
#else
#define DEBUG_MMH_MSG(str) do { } while ( false )
#endif

class MultimodalModularHeuristic {
private:
    //Exterior members that the algorithm references or acts upon
    const Graph* _graph{nullptr};
    const std::vector<Request>* _requestsVect{nullptr};
    SAEVRoute* _route{nullptr}; //_route is a pointer here to decorrelate the route from the algorithm and facilitate using multiple modules on the same route

    //TODO: Move the diverse components (min/max constraints, get best entries/exits list etc) to their own classes to be used as delegates/decorator-like pattern
    //      This would have the added benefit of easier customisation. Just have a common "apply()" function, and give useful data in constructor
    //      => every inheritor just has an apply() method, used here, returning the appropriate data,
    //         but child class constructor can be free to have all the data we want

    /**
     * Vector holding every entry sub request created to prevent refs staleness
     * while letting the requests vector hold only the base requests \n
     * \n
     * Initialized with empty requests to match the number of base requests in the constructor
     */
    std::vector<Request> _entrySubRequests;
    /**
     * Vector holding every exit sub request created to prevent refs staleness
     * while letting the requests vector hold only the base requests\n
     * \n
     * Initialized with empty requests to match the number of base requests in the constructor
     */
    std::vector<Request> _exitSubRequests;

    /**
     * Marks unfulfilled entry requests due to not finding a single viable candidate
     */
    std::vector<bool> _unfulfilledTransitEntry;
    /**
     * Marks unfulfilled exit requests due to not finding a single viable candidate
     */
    std::vector<bool> _unfulfilledTransitExit;

    /**
     * Vector containing every base request (referenced via its _requestsVect index) that needs to be fulfilled in SAEV-only DARP \n
     * \n
     * This includes non-multimodal requests and requests that couldn't be fulfilled in the entry/exit step of the multimodal procedure
     */
    std::vector<size_t> _darpRequestList;

    //Add friend test classes to test inner workings without making the whole API public
    FRIEND_TEST(MultimodalInsertionHeuristicDebug, DebugPTInstance);
    FRIEND_TEST(MultimodalInsertionHeuristicDebug, DebugInstanceAlain);

//Public interface to interact with the modular heuristic
public:
    MultimodalModularHeuristic(const Graph *graph, SAEVRoute *route, std::vector<Request> const* requestsVect) : _graph(graph), _requestsVect(requestsVect),
    _route(route), _entrySubRequests(requestsVect->size()), _exitSubRequests(requestsVect->size()),
    _unfulfilledTransitEntry(requestsVect->size()), _unfulfilledTransitExit(requestsVect->size()) {}

    /**
     * Executes iteratively each steps of the algorithm :
     * <ol>
     *  <li> Iterate over all base requests to generate and insert transit entry sub-request </li>
     *  <li> From those inserted entry sub-requests, find compatible exit candidates and try to insert them </li>
     *  <li> Insert every non-multimodal base request or previously unsuccessful multimodal base request for which step 1 or 2 failed </li>
     * </ol>
     *
     * If we fail step 1 or 2 for a multimodal request or the request is non-multimodal,
     * it's added to a list of DARP requests to be fulfilled \n
     * If we fail step 2 (no compatible exit available), remove the entry request and reset+propagate bounds
     */
    void doMultimodalRequestsInsertion();

    [[nodiscard]] size_t getNbBaseRequests() const {
        return _requestsVect->size();
    }

    [[nodiscard]] const std::vector<Request> &getEntrySubRequests() const;
    [[nodiscard]] const std::vector<Request> &getExitSubRequests() const;

    [[nodiscard]] const std::vector<bool> &getUnfulfilledTransitEntry() const;
    [[nodiscard]] const std::vector<bool> &getUnfulfilledTransitExit() const;

    //Define useful struct to order transit access objects
    struct ScoredTransitAccess : public TransitAccess {
        double score{DBL_MAX};
        explicit ScoredTransitAccess(const TransitAccess& access, double scr) : TransitAccess(access), score(scr) {}
    };
    using transit_order_function = std::function<bool(ScoredTransitAccess, ScoredTransitAccess)>;

//Private members for heuristic internal functions we don't wish to see overriden
private:

    const Request & insertBestTransitEntryInRoute(const Request &baseRequest, size_t baseRequestId);
    const Request & insertBestTransitExitsInRoute(const Request &baseRequest, size_t baseRequestId);


    const Request &insertBestTransitAccessInRoute(const Request &baseRequest, const std::vector<TransitAccess> &transitAccessList,
                                   size_t baseRequestId, bool isEntry);
    const Request &insertBestTransitAccessInRoute(const std::vector<Request> &accessSubRequestsList,
                                                  size_t baseRequestId, bool isEntry);

    //Best candidates function
    /**
     * Creates and returns a vector of TransitAccess objects representing possible
     * transit entries that can be converted to Request objects to try and insert them
     * in a vehicle. <br>
     * <br>
     * This vector is sorted in the same way as Node._bestStationsNodeIdxVector and has max size
     * Constants::MAX_TRANSIT_ENTRY_CANDIDATES. There can be less or even 0 elements if no candidate
     * has a transit departure tmax in getMinEntryConstraint <= tmax <= getMaxEntryConstraint
     *
     * @param baseRequest the request we wish to find potential transit entries for
     * @return A vector consisting of all valid TransitAccess objects wrt the base request
     * and the min/max exit constraints. If no valid access is found, we return an empty vector.
     */
    [[nodiscard]] std::vector<TransitAccess> getBestTransitEntriesList(const Request &baseRequest) const;

    /**
     * Creates and returns a vector of TransitAccess objects representing possible
     * transit entries that can be converted to Request objects to try and insert them
     * in a vehicle. <br>
     * <br>
     * This vector is sorted according to the sort function the class was initialized with
     * (default = t' + T(y,Dr)). It does not guarantee a max size of Constants::MAX_TRANSIT_EXIT_CANDIDATES.
     * There can be less, or even 0 elements if no candidate has a transit departure tmax in
     * getMinEntryConstraint <= tmax <= getMaxEntryConstraint
     *
     * @param baseRequestId id of the base request for which we look for best exits
     * @return A vector consisting of all valid TransitAccess objects wrt the entry request's state
     * and the min/max entry constraints. If no valid access is found, we return an empty vector.
     */
    std::vector<TransitAccess> getBestTransitExitsList(size_t baseRequestId);
    /**
    * Creates and returns a vector of TransitAccess objects representing possible
    * transit entries that can be converted to Request objects to try and insert them
    * in a vehicle. <br>
    * <br>
    * This vector is sorted according to the sort function the class was initialized with
    * (default = t' + T(y,Dr)). It does not guarantee a max size of Constants::MAX_TRANSIT_EXIT_CANDIDATES.
    * There can be less, or even 0 elements if no candidate has a transit departure tmax in
    * getMinEntryConstraint <= tmax <= getMaxEntryConstraint
    *
    * @param baseRequestId id of the base request for which we look for best exits
    * @return A vector consisting of all valid TransitAccess objects wrt the entry request's state
    * and the min/max entry constraints. If no valid access is found, we return an empty vector.
    */
    [[nodiscard]] std::vector<TransitAccess>
    getBestTransitExitsList(size_t baseRequestId, const Request &baseRequest,
                            const SAEVKeyPoint &entrySubRequestOriginKP) const;

//Protected member function for overriding as we make this more modular
protected:
    //Entry filter
    [[nodiscard]] uint getMinEntryConstraint(const Request &request, size_t ptEntryNodeIdx) const;
    [[nodiscard]] uint getMaxEntryConstraint(const Request &request, size_t ptEntryNodeIdx) const;
    //Exit filter
    /**
     * Base implementation of min exit constraint.
     * This implementation is equivalent to no min constraint, as our subset of possible moves already filters our options a bit
     * @param baseRequestId Id of the base request for which we generate exit subrequests. Necessary to get data on base request and entry subrequest if necessary
     * @param transitExitNodeIdx exit node index
     * @return 0
     */
    [[nodiscard]] uint getMinExitConstraint(size_t baseRequestId, const SAEVKeyPoint &entrySubRequestOriginKP,
                                            size_t transitExitNodeIdx) const;
    /**
     * Base implementation of max exit constraint.
     * This base implementation just checks for arrival validity wrt the base request's max time window
     * @param baseRequestId Id of the base request for which we generate exit subrequests. Necessary to get data on base request and entry subrequest if necessary
     * @param transitExitNodeIdx exit node index
     * @return baseRequest.DestinationTW.max - T(exitData.Node, baseRequest.Destination)
     */
    [[nodiscard]] uint getMaxExitConstraint(size_t baseRequestId, const SAEVKeyPoint &entrySubRequestOriginKP,
                                            size_t transitExitNodeIdx) const;
    /**
     * Base implementation of a sorting score (lower is better) for exit candidates.
     * This implementation scores via T(exitNode, destinationNode) + exitTime to try and
     * incentivize early and/or close access points. <br> <br>
     * Override this function to alter exit candidates ordering via the score function <br>
     * /!\ Be mindful that vectors are sorted in ascending order, so invert the score or override getScoredTransitExitOrderer if you want greater values first /!\
     * @param baseRequest the base request, required to get the destination
     * @param exitData exit data containing the exit point and timestamp
     * @return A score allowing to sort transit exits in ascending score order
     */ //TODO: try other scoring functions (current other idea : T(exitNode, destinationNode) * exitTime; alpha*T(exitNode, destinationNode) + exitTime)
    [[nodiscard]] double getTransitExitScore(const Request& baseRequest, const TransitAccess& exitData) const;
    /**
     * This function returns an orderer function used to sort the transit exit priority queue. <br> <br>
     * Only override this function if you need a more involved kind of sort function that can't be made by simply overriding getTransitExitScore. <br>
     * /!\ Be mindful that vectors are sorted in ascending order, so invert the order if you want greater values first /!\
     * @return Returns a function taking in argument two ScoredTransitAccess objects and returning a true if lhs is to be ordered before rhs, false otherwise
     */
    [[nodiscard]] static transit_order_function getScoredTransitExitOrderer() ;

    //Keep those getters/setters protected as no other member should access or modify these objects
    [[nodiscard]] const Graph *getGraph() const;
    void setGraph(const Graph *graph);

    [[nodiscard]] SAEVRoute *getRoute() const;
    void setRoute(SAEVRoute *route);

    void updateSubRequest(size_t requestId, const Request &request, bool isEntry);

    const Request &getSubrequest(size_t requestId, bool isEntry);

    [[nodiscard]] double getTransitExitScore(size_t transitExitNodeIndex, size_t requestDestinationNodeIndex,
                               uint transitExitTimestamp) const;

    void updateUnfulfilledSubrequest(size_t baseRequestId, bool isEntry, bool value);

    bool isEntryFulfilled(size_t baseRequestId);

    void emplaceDARPRequest(size_t baseRequestId);
};


#endif //GREEDYALGORITHM_MULTIMODALMODULARHEURISTIC_H
